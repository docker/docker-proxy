#!/usr/bin/env bash

# Domain should be the first argument
[ -z "$1" ] && echo "Usage: ./bin/mkcert.sh DOMAIN" && exit 1

# Set DOMAIN
DOMAIN=$1

# Check if mkcert is installed
if ! command -v mkcert &>/dev/null; then
  echo "Error: mkcert is not installed. Please install mkcert."
  exit 1
fi

# Check if mkcert is configured
if ! mkcert -CAROOT &>/dev/null; then
  echo "Error: mkcert is not configured. Please run 'mkcert -install' to configure mkcert."
  exit 1
fi

# ensure certs directory exists
mkdir -p certs

# check if certificate already exists
if [ -f certs/$DOMAIN.crt ]; then
  echo "Error: certs/$DOMAIN.crt already exists."
  exit 1
fi

# check if key already exists
if [ -f certs/$DOMAIN.key ]; then
  echo "Error: certs/$DOMAIN.key already exists."
  exit 1
fi

# Create certificates for DOMAIN and *.DOMAIN
mkcert -cert-file certs/$DOMAIN.crt -key-file certs/$DOMAIN.key $DOMAIN "*.$DOMAIN"

# if dynamic directory does not exist, create it
if [ ! -d dynamic ]; then
  mkdir -p dynamic
fi

# if dynamic/certs.toml does not exist, create it
if [ ! -f dynamic/certs.toml ]; then
  touch dynamic/certs.toml
fi

# add to dynamic config
echo "[[tls.certificates]]
certFile = \"/certs/$DOMAIN.crt\"
keyFile = \"/certs/$DOMAIN.key\"" >>dynamic/certs.toml
