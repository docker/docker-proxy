# Ensure DOMAIN is provided as the first argument
if ($args.Count -eq 0) {
    Write-Host "Usage: .\mkcert.ps1 DOMAIN"
    exit 1
}

# Set DOMAIN
$DOMAIN = $args[0]

# Check if mkcert is installed
if (-not (Get-Command "mkcert" -ErrorAction SilentlyContinue)) {
    Write-Host "Error: mkcert is not installed. Please install mkcert."
    exit 1
}

# Check if mkcert is configured
if (-not (mkcert -CAROOT 2>$null)) {
    Write-Host "Error: mkcert is not configured. Please run 'mkcert -install' to configure mkcert."
    exit 1
}

# Ensure certs directory exists
$certsDir = "certs"
if (-not (Test-Path $certsDir)) {
    New-Item -ItemType Directory -Path $certsDir
}

# Check if certificate already exists
if (Test-Path "$certsDir\$DOMAIN.crt") {
    Write-Host "Error: certs/$DOMAIN.crt already exists."
    exit 1
}

# Check if key already exists
if (Test-Path "$certsDir\$DOMAIN.key") {
    Write-Host "Error: certs/$DOMAIN.key already exists."
    exit 1
}

# Create certificates for DOMAIN and *.DOMAIN
mkcert -cert-file "$certsDir\$DOMAIN.crt" -key-file "$certsDir\$DOMAIN.key" $DOMAIN "*.$DOMAIN"

# Ensure dynamic directory exists
$dynamicDir = "dynamic"
if (-not (Test-Path $dynamicDir)) {
    New-Item -ItemType Directory -Path $dynamicDir
}

# Ensure dynamic/certs.toml exists
$certsToml = "$dynamicDir\certs.toml"
if (-not (Test-Path $certsToml)) {
    New-Item -ItemType File -Path $certsToml
}

# Add to dynamic config
$certConfig = @"
[[tls.certificates]]
certFile = "/certs/$DOMAIN.crt"
keyFile = "/certs/$DOMAIN.key"
"@
Add-Content -Path $certsToml -Value $certConfig
