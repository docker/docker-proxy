# Traefik Docker Proxy Setup

This is a sample configuration for setting up Traefik as a Docker proxy.

## Prerequisites

- Docker installed on your machine

## Usage

1. Clone this repository:

```shell
git clone git@gitlab.msu.edu:docker/docker-proxy.git
```

2. Navigate to the project directory:

```shell
cd docker-proxy
```

3. Start the Traefik container:

```shell
docker-compose up -d
```

4. Setup mkcert:

- Note: mkcert is a tool for generating trusted SSL certificates for local development. You can read more about it [here](https://github.com/FiloSottile/mkcert).

#### For macOS

```shell
# to install mkcert on macOS
brew install mkcert
brew install nss # if you use Firefox

# initialize mkcert
mkcert -install

# generate a cert for traefik.localhost
./bin/mkcert.sh traefik.localhost
```

#### For Windows

```powershell
choco install mkcert # use choco or another method to install on Windows

# initialize mkcert
mkcert -install

# generate a cert using ps1
./bin/mkcert.ps1 traefik.localhost
```

5. Browse to https://traefik.localhost/dashboard/ to see the Traefik dashboard.
